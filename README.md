# Fast Library for Approximate Nearest Neighbors (Java port)

<br>

I ported FLANN from C++ to Java as a semester project for my master studies (2012/2013) at<br>
"University of Applied Sciences Upper Austria, Campus Hagenberg".<br>
I was also getting feedback from the author of the original FLANN library,
Marius Muja.

<br>

| User | Organization |
| ------ | ------ |
| Nikolay Samusik | Stanford University |